#lang sicp
(define (A x y)
  (cond
    ((= y 0) 0)
    ((= x 0) (* 2 y))
    ((= y 1) 2)
    (else (A (- x 1) (A x (- y 1))))))

(define (f n) (A 0 n))
(define (g n) (A 1 n))
(define (h n) (A 2 n))
(define (k n) (* 5 n n))

(define (f-r n)
  (cond
    ((< n 3) n)
    ((+ (f-r (- n 1)) (* 2 (f-r (- n 2))) (* 3 (f-r (- n 3)))))
  )
)

(define (f-iter n k fm1 fm2 fm3)
  (cond
    ((< n 3) n)
    ((= (+ n 1) k) fm1)
    (else (f-iter n (+ k 1) (+ fm1 (* 2 fm2) (* 3 fm3)) fm1 fm2))
  )
)

(define (f-i n)
  (f-iter n 3 2 1 0))

(define (n-over-k n k)
  (cond
    ((= k 0) 1)
    ((= n 0) 0)
    (else (+ (n-over-k (- n 1) (- k 1)) (n-over-k (- n 1) k)))
  )
)

(define (pascals-triangle-element row number)
  (n-over-k row number))

(define (square x) (* x x))
(define (cube x) (* x x x))
(define (p x) (- (* 3 x) (* 4 (cube x))))
(define (sine angle)
  (if (not (> (abs angle) 0.1))
    angle
    (p (sine (/ angle 3.0)))))

(define (expt b n)
  (if (= n 0)
    1
    (* b (expt b (- n 1)))))

(define (expt-i b n)
  (expt-iter b n 1))

(define (expt-iter b counter product)
  (if (= counter 0)
      product
      (expt-iter b (- counter 1) (* b product))))

(define (fast-expt b n)
(cond ((= n 0) 1)
      ((even? n) (square (fast-expt b (/ n 2))))
      (else (* b (fast-expt b (- n 1))))))

(define (even? n)
  (= (remainder n 2) 0))

(define (fast-expt-i b n)
  (define (fast-expt-iterative b n a rem)
  (cond
    ((= n 0) 1)
    ((= b 1) 1)
    ((= b 0) 0)
    ((= n 1) (* a rem))
    ((even? n) (fast-expt-iterative b (/ n 2) (square a) rem))
    (else (fast-expt-iterative b (- n 1) a (* rem a)))))
  (fast-expt-iterative b n b 1))

(define (double x) (* x 2))
(define (halve x) (/ x 2))

(define (fast-mul-r a b)
  (cond
    ((= a 0) b)
    ((= b 1) a)
    ((even? b) (double (fast-mul-r a (halve b))))
    (else (+ a (fast-mul-r a (- b 1))))))

(define (fast-mul-i a b)
  (define (fast-mul-iterative a b sum)
  (cond
    ((= a 0) b)
    ((< b 2) (+ a sum))
    ((even? b) (fast-mul-iterative (double a) (halve b) sum))
    (else (fast-mul-iterative a (- b 1) (+ sum a)))))
  (fast-mul-iterative a b 0))

(define (fib n)
  (fib-iter 1 0 0 1 n))
(define (fib-iter a b p q count)
  (cond
    ((= count 0) b)
    ((even? count) (fib-iter
                    a
                    b
                    (+ (square q) (square p)) ; compute p′
                    (+ (square q) (* 2 q p)) ; compute q′
                    (/ count 2)))
    (else (fib-iter (+ (* b q) (* a q) (* a p))
                    (+ (* b p) (* a q))
                    p
                    q
                    (- count 1)))))

(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))

(define (smallest-divisor n) (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond (
         (> (square test-divisor) n) n)
         ((divides? test-divisor n) test-divisor)
         (else (find-divisor n (next test-divisor)))))

(define (divides? a b) (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (expmod base exp m)
  (cond
    ((= exp 0) 1)
    ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
    (else (remainder (* base (expmod base (- exp 1) m)) m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond
    ((= times 0) true)
    ((fermat-test n) (fast-prime? n (- times 1)))
    (else false)))

(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (search-for-primes first last remaining)
  (timed-prime-test first)
  (cond
    ((even? first) (search-for-primes (+ first 1) last remaining))
    ((or (> first last) (= remaining 0)) first)
    (else (search-for-primes (+ first 2) last remaining))))

(define (next input)
  (cond
    ((= input 2) 3)
    (else (+ input 2))))


(define (timed-fast-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-fast-prime-test n start-time)
  (if (fast-prime? n)
      (report-prime (- (runtime) start-time))))

(define (search-for-fast-primes first last remaining)
  (timed-fast-prime-test first)
  (cond
    ((even? first) (search-for-fast-primes (+ first 1) last remaining))
    ((or (> first last) (= remaining 0)) first)
    (else (search-for-fast-primes (+ first 2) last remaining))))


(define (expmod-m base exp n)
  (define (check-square k n)
  (cond
    ((and (not (= k 1)) (not (= k (- n 1))) (= (remainder (square k) n) 1)) 0)
    (else (remainder (square k) n))))
  (cond
    ((= exp 0) 1)
    ((even? exp) (check-square (expmod-m base (/ exp 2) n) n))
    (else (remainder (* base (expmod-m base (- exp 1) n)) n))))




(define (miller-rabin n)
  (define (try-it a)
    (= (expmod-m a (- n 1) n) 1))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime-mr? n times)
   (cond
    ((= times 0) true)
    ((miller-rabin n) (fast-prime-mr? n (- times 1)))
    (else false)))